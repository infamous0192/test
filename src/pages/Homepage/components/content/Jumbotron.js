import React from 'react'
import { Col, Container, Row } from 'reactstrap'

const Jumbotron = () => {
  const style = {
    background: {backgroundImage: 'url(./img/intro-bg.jpg)'},
    introBg: "./img/intro-img.png"
  }

  return (
    <div id="intro" className="min-vh-100 d-flex align-items-center text-center text-md-left" style={style.background}>
      <Container>
        <Row>
          <Col md="7" className="pr-md-5 ">
            <h1 className="font-weight-bold">Kami Hadir untuk kemudahan Laundry Anda</h1>
            <p className="pt-4">Kami hadir untuk memberikan layanan laundry untuk Anda dengan berorientasi pada kemudahan dan kualitas pada setiap produk jasa yang kami tawarkan.</p>

            <button type="button" className="btn btn-primary btn-lg rounded-pill py-3 px-5 shadow-none mt-3">Pesan Sekarang</button>
          </Col>
          <Col md="5" className="intro-img mx-auto">
            <img src={style.introBg} alt="" className="w-100 h-auto pt-4"/>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Jumbotron