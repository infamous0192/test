import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  Container,
} from "reactstrap";

const Navigasi = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Navbar light expand="md" className="fixed-top py-md-4">
      <Container className="">
        <NavbarBrand href="/">
          <img src="/img/logo_dark.png" alt="" />
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="/">Beranda</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/">Komitmen</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/">Sekilas</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/">Kontak</NavLink>
            </NavItem>
            <NavItem className="">
              <button type="button" className="btn btn-outline-success px-5 px-md-4 ml-4 rounded-pill shadow-none my-2 my-md-0">Masuk</button>
            </NavItem>
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  );
};

export default Navigasi;
