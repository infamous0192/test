import React, { Component } from 'react'
import JumboBox from './components/content/Jumbotron'
import Navbar from './components/layout/Navbar'

class Homepage extends Component {
  render() {  
    return (
      <>
        <Navbar />
        <JumboBox />
      </>
    )
  }
}

export default Homepage

